const randomString = require('../index');

describe('Probar funcionalidades de randomString', () => {
    test('Probar una funcion random', () => {
        expect(typeof (randomString())).toBe('string');
    });

    test('Comprobar que no existe una ciudad de Colombia', () => {
        expect(randomString()).not.toMatch(/cordoba/);
    });
});