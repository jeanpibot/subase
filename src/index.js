const cities = ['Neiva', 'Florencia', 'Medellin', 'Bogota', 'Pereira', 'Cali', 'manizales'];

// funcion para devolver una ciudad aleatoria
const randomString = () => {
    const city = cities[Math.floor(Math.random() * cities.length)];
    return city;
};

module.exports = randomString;